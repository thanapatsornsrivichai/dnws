using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using System.Threading;
using DNWS1;

namespace DNWS
{
    // Main class
    public class Program
    {
        static public IConfigurationRoot Configuration { get; set; }

        // Log to console
        public void Log(String msg)
        {
            Console.WriteLine(msg);
        }

        // Start the server, Singleton here
        public void Start()
        {
            // Start server
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("config.json");
            Configuration = builder.Build();
            DotNetWebServer ws = DotNetWebServer.GetInstance(Convert.ToInt16(Configuration["Port"]), this);
            ws.Start();
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.Start();
        }
    }

    /// <summary>
    /// HTTP processor will process each http request
    /// </summary>

    public class HTTPProcessor
    {
        // Get config from config manager, e.g., document root and port
        protected string ROOT = Program.Configuration["DocumentRoot"];
        protected Socket _client;
        protected Program _parent;
        protected static Dictionary<String, int> statDictionary = null;

        /// <summary>
        /// Constructor, set the client socket and parent ref, also init stat hash
        /// </summary>
        /// <param name="client">Client socket</param>
        /// <param name="parent">Parent ref</param>
        public HTTPProcessor(Socket client, Program parent)
        {
            _client = client;
            _parent = parent;
            if (statDictionary == null)
            {
                statDictionary = new Dictionary<String, int>();

            }
        }

        /// <summary>
        /// Increment counting for the url
        /// </summary>
        /// <param name="url">target url</param>
        public void incr(string url)
        {
            if (statDictionary.ContainsKey(url))
            {
                statDictionary[url] = (int)statDictionary[url] + 1;
            }
            else
            {
                statDictionary[url] = 1;
            }
        }

        /// <summary>
        /// Gen HTML stat table
        /// </summary>
        /// <returns>HTML stat table</returns>
        public string genStat()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html><body><h1>Stat:</h1>");
            foreach (KeyValuePair<String, int> entry in statDictionary)
            {
                sb.Append(entry.Key + ": " + entry.Value.ToString() + "<br />");
            }
            sb.Append("</body></html>");
            return sb.ToString();
        }

        public string showinfo(String user_agent, String accept_language, String accept_enco)
        {
            StringBuilder info = new StringBuilder();
            string IP = _client.RemoteEndPoint.ToString();
            //SPLIT UP IP AND PORT
            string[] words = IP.Split(':');
            string info_user_agent = user_agent.Remove(0, 11);

            info.Append("<!DOCTYPE html><html><head></head><body>");
            info.Append("Client IP: " + words[0] + "<br><br>"); 
            info.Append("Client Port: " + words[1] + "<br><br>"); 
            info.Append("Browser Information: " + info_user_agent + "<br><br>");
            info.Append(accept_language + "<br><br>");
            info.Append(accept_enco);
            info.Append("</body></html>");
            return info.ToString();
        }

        /// <summary>
        /// Get a file from local harddisk based on path
        /// </summary>
        /// <param name="path">Absolute path to the file</param>
        /// <returns></returns>
        protected HTTPResponse getFile(String path)
        {
            HTTPResponse response = null;

            // Guess the content type from file extension
            string fileType = "text/html";
            if (path.ToLower().EndsWith("jpg") || path.ToLower().EndsWith("jpeg"))
            {
                fileType = "image/jpeg";
            }
            if (path.ToLower().EndsWith("png"))
            {
                fileType = "image/png";
            }

            // Try to read the file, if not found then 404, otherwise, 500.
            try
            {
                response = new HTTPResponse(200);
                response.type = fileType;
                response.body = System.IO.File.ReadAllBytes(path);
            }
            catch (FileNotFoundException ex)
            {
                response = new HTTPResponse(404);
                response.body = Encoding.UTF8.GetBytes("<h1>404 Not found</h1>" + ex.Message);
            }
            catch (Exception ex)
            {
                response = new HTTPResponse(500);
                response.body = Encoding.UTF8.GetBytes("<h1>500 Internal Server Error</h1>" + ex.Message);
            }
            return response;

        }

        /// <summary>
        /// Get a request from client, process it, then return response to client
        /// </summary>


        public void Process()
        {
            NetworkStream ns = new NetworkStream(_client);
            string request = "";
            HTTPResponse response = null;
            byte[] bytes = new byte[1024];
            int bytesRead;

            string user_agent = "";
            string accept_language = "";
            string accept_enco = "";

            int start_BrowserInfo;
            int end_BrowserInfo;
            int length_BrowserInfo;

            int start_AcceptLanguage;
            int end_AcceptLanguge;
            int length_AcceptLanguge;

            int start_AcceptEnco;
            int end_AcceptEnco;
            int length_AcceptEnco;

            // Read all request
            do
            {
                bytesRead = ns.Read(bytes, 0, bytes.Length); 
                request += Encoding.UTF8.GetString(bytes);


                //Console.WriteLine(request.IndexOf("Accept")); use for Debug...
                //Console.WriteLine(request);

            } while (ns.DataAvailable);

            // We can handle only GET now   
            String[] tokens = Regex.Split(request, "\\s");
            if (tokens.Length < 1)
            {
                response = new HTTPResponse(500);
            }
            else
            {
                if (tokens.Length < 2)
                {
                    response = new HTTPResponse(500);
                }
                else if (!tokens[0].ToLower().Equals("get"))
                {
                    response = new HTTPResponse(501);
                }
                else
                {
                    String[] subTokens = Regex.Split(tokens[1], "/");
                    if (tokens[1].ToLower().EndsWith("/stat"))
                    {
                        response = new HTTPResponse(200);
                        response.body = Encoding.UTF8.GetBytes(genStat());
                    }
                    else if (tokens[1].ToLower().StartsWith("/client"))
                    {
                        response = new HTTPResponse(200);
                        if (request.IndexOf("Accept:") > 200)
                        {
                            start_BrowserInfo = request.IndexOf("User-Agent:"); 
                            end_BrowserInfo = request.LastIndexOf("Accept:"); 
                            length_BrowserInfo = end_BrowserInfo - start_BrowserInfo; 
                            user_agent = request.Substring(start_BrowserInfo, length_BrowserInfo); 

                            start_AcceptLanguage = request.IndexOf("Accept-Language:"); 

                            if (request.IndexOf("Cookie:") != -1)
                            { 
                                end_AcceptLanguge = request.LastIndexOf("Cookie:"); 
                                length_AcceptLanguge = end_AcceptLanguge - start_AcceptLanguage;
                                accept_language = request.Substring(start_AcceptLanguage, length_AcceptLanguge);
                            }
                            //ANDROID
                            else
                            { 
                                end_AcceptLanguge = request.LastIndexOf("0."); 
                                length_AcceptLanguge = end_AcceptLanguge - start_AcceptLanguage; 
                                accept_language = request.Substring(start_AcceptLanguage, length_AcceptLanguge + 3); 
                            }

                            start_AcceptEnco = request.IndexOf("Accept-Encoding:"); 
                            end_AcceptEnco = request.LastIndexOf("Accept-Language:");
                            length_AcceptEnco = end_AcceptEnco - start_AcceptEnco;
                            accept_enco = request.Substring(start_AcceptEnco, length_AcceptEnco);
                            response.body = Encoding.UTF8.GetBytes(showinfo(user_agent, accept_language, accept_enco));
                        }

                        //EDGE
                        else if (request.IndexOf("Accept:") < 50)
                        {
                            start_BrowserInfo = request.IndexOf("User-Agent:"); 
                            end_BrowserInfo = request.LastIndexOf("Accept-Encoding:");
                            length_BrowserInfo = end_BrowserInfo - start_BrowserInfo; 
                            user_agent = request.Substring(start_BrowserInfo, length_BrowserInfo);

                            start_AcceptLanguage = request.IndexOf("Accept-Language:"); 
                            end_AcceptLanguge = request.LastIndexOf("User-Agent:"); 
                            length_AcceptLanguge = end_AcceptLanguge - start_AcceptLanguage; 
                            accept_language = request.Substring(start_AcceptLanguage, length_AcceptLanguge + 3); 

                            start_AcceptEnco = request.IndexOf("Accept-Encoding:"); 
                            end_AcceptEnco = request.LastIndexOf("Host:"); 
                            length_AcceptEnco = end_AcceptEnco - start_AcceptEnco; 
                            accept_enco = request.Substring(start_AcceptEnco, length_AcceptEnco); 
                            response.body = Encoding.UTF8.GetBytes(showinfo(user_agent, accept_language, accept_enco));

                        }

                        //SAFARI
                        else if (request.IndexOf("Accept:") < 100) 
                        {
                            start_BrowserInfo = request.IndexOf("User-Agent:"); 
                            end_BrowserInfo = request.LastIndexOf("Accept-Language:");
                            length_BrowserInfo = end_BrowserInfo - start_BrowserInfo;
                            user_agent = request.Substring(start_BrowserInfo, length_BrowserInfo); 

                            start_AcceptLanguage = request.IndexOf("Accept-Language:"); 
                            end_AcceptLanguge = request.LastIndexOf("Accept-Encoding:");
                            length_AcceptLanguge = end_AcceptLanguge - start_AcceptLanguage; 
                            accept_language = request.Substring(start_AcceptLanguage, length_AcceptLanguge); 

                            start_AcceptEnco = request.IndexOf("Accept-Encoding:"); 
                            end_AcceptEnco = request.LastIndexOf("Connection:"); 
                            length_AcceptEnco = end_AcceptEnco - start_AcceptEnco; 
                            accept_enco = request.Substring(start_AcceptEnco, length_AcceptEnco); 
                            response.body = Encoding.UTF8.GetBytes(showinfo(user_agent, accept_language, accept_enco));
                        }
                        

                    }

                    //GAME
                    else if (subTokens[subTokens.Length - 1].ToLower().StartsWith("ox"))
                    {
                        OXGame game = OXGame.GetInstance();
                        response = game.Process(subTokens[subTokens.Length - 1]);
                    }
                    // Then it's a file request
                    else
                    {
                        if (tokens[1].EndsWith("/"))
                        {
                            response = getFile(ROOT + "/index.html");
                        }
                        else
                        {
                            response = getFile(ROOT + tokens[1]);
                        }
                        incr(tokens[1]);
                    }
                }
            }


            // For testing purpose
            // Thread.Sleep(1000);
            // Generate response
            ns.Write(Encoding.UTF8.GetBytes(response.header), 0, response.header.Length);
            if (response.body != null)
            {
                ns.Write(response.body, 0, response.body.Length);
            }

            // Shuting down
            //ns.Close();
            _client.Shutdown(SocketShutdown.Both); // shutdown จาก server
            //_client.Close();

        }
    }

    /// <summary>
    /// Main server class, open the socket and wait for client
    /// </summary>
    public class DotNetWebServer
    {
        protected int _port;
        protected Program _parent;
        protected Socket serverSocket;
        protected Socket clientSocket;
        private static DotNetWebServer _instance = null;
        protected int id;

        private DotNetWebServer(int port, Program parent)
        {
            _port = port;
            _parent = parent;
            id = 0;
        }

        /// <summary>
        /// Singleton here
        /// </summary>
        /// <param name="port">Listening port</param>
        /// <param name="parent">parent ref</param>
        /// <returns></returns>
        public static DotNetWebServer GetInstance(int port, Program parent)
        {
            if (_instance == null)
            {
                _instance = new DotNetWebServer(port, parent);
            }
            return _instance;
        }

        /// <summary>
        /// Server starting point
        /// </summary>
        public void Start()
        {
            try
            {
                // Create listening socket, queue size is 5 now.
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, _port);
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(localEndPoint);
                serverSocket.Listen(5);
                _parent.Log("Server started at port " + _port + ".");
            }
            catch (Exception ex)
            {
                _parent.Log("Server started unsuccessfully.");
                _parent.Log(ex.Message);
                return;
            }
            while (true)
            {
                try
                {
                    // Wait for client
                    clientSocket = serverSocket.Accept(); // Listen
                    // Get one, show some info
                    _parent.Log("Client accepted:" + clientSocket.RemoteEndPoint.ToString());
                    HTTPProcessor hp = new HTTPProcessor(clientSocket, _parent);
                    // Single thread
                    //hp.Process();
                    // End single therad

                    // Multi thread
                    Thread Thread = new Thread(new ThreadStart(hp.Process));
                    Thread.Name = "id:" + id++;
                    //_parent.Log("Start thread with name " + Thread.Name);
                    Thread.Start();
                    // End multi thread
                }
                catch (Exception ex)
                {
                    _parent.Log("Server starting error: " + ex.Message + "\n" + ex.StackTrace);

                }
            }

        }
    }
}